package com.example.battleapp;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class GameSettingActivity extends UserActivity {

    private DatabaseReference userDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(getUserId());
    private TextView bestScoreTv;
    private Spinner difficultySpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_setting);

        // INITIALIZING WIDGETS REFS
        ImageButton backButton = findViewById(R.id.settings_back_button);
        Button startGameButton = findViewById(R.id.start_game_button);
        difficultySpinner = findViewById(R.id.spinner_difficulty);
        bestScoreTv = findViewById(R.id.best_score_tv);

        // SETTING ON CLICK LISTENERS
        startGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gameActivityIntent = new Intent(getApplicationContext(), GameActivity.class);

                int difficultyLevel;

                switch (difficultySpinner.getSelectedItem().toString()){
                    case "EASY" :
                        difficultyLevel = 1;
                        break;
                    case "MEDIUM" :
                        difficultyLevel = 2;
                        break;
                    case "EXTREME" :
                    default:
                        difficultyLevel = 3;
                }
                //  SEND DATA TO ANOTHER ACTIVITY
                gameActivityIntent.putExtra("difficulty", difficultyLevel);
                startActivity(gameActivityIntent);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PreGameActivity.class));
            }
        });

        DisplayScore(); // DISPLAY USER SCORE
    }

    /**
     * GET ACTUAL USER SCORE AND DISPLAYS IT
     */
    private void DisplayScore(){
        try {
            userDatabaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    try {
                        final int score = - Integer.parseInt(dataSnapshot.child("score").getValue().toString());
                        bestScoreTv.setText("YOUR SCORE : " + Integer.toString(score));
                    }
                    catch (NullPointerException ex)
                    {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }catch (Exception ex)
        {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
