package com.example.battleapp;

import androidx.appcompat.app.AppCompatActivity;

/**
 * THIS CLASS ENABLES TO ACCESS USER ID FROM ANYWHERE IN THE APP
 */
public abstract class UserActivity extends AppCompatActivity {

    private static String userId;

    public void setUserId(String id){
        userId = id;
    }

    public static String getUserId() {
        return userId;
    }
}
