package com.example.battleapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class RanksActivity extends AppCompatActivity {

    private DatabaseReference userDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users");
    private TextView rankTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranks);

            // INITIALIZING WIDGETS REFS
            rankTextView = findViewById(R.id.rank_tv);

            final Query scoreQuery = userDatabaseReference.orderByChild("score").limitToFirst(10);

            // DISPLAY BEST USERS ACCORDING TO SCORES
            scoreQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    int userScore = - Integer.parseInt(dataSnapshot.child("score").getValue().toString());
                    rankTextView.append(dataSnapshot.child("name").getValue().toString() + " " + dataSnapshot.child("lastName").getValue().toString() + " : " + Integer.toString(userScore) + "\n");
                    rankTextView.append("--------------\n");
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
    }
}
