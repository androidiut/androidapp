package com.example.battleapp;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

public class GameActivity extends UserActivity {

    private DatabaseReference userDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(getUserId());
    private TextView roundTextView, pointsTextViex;
    private ImageButton paperButton, scissorsButton, rockButton, spockButton, lizardButton;
    private int turnNumber = 1, nbpoints = 0, pointsGained;
    private boolean hasBeenAdded = false;
    private double probaWin, probaDraw;
    private ImageView enemyChoice;
    private SignEnum playerSign;
    private Random randomSign = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // INITIALIZING WIDGETS REFS
        TextView gameDifficultyTextView = findViewById(R.id.game_difficulty_tv);
        roundTextView = findViewById(R.id.round_tv);
        pointsTextViex = findViewById(R.id.points_tv);
        paperButton = findViewById(R.id.paperButton);
        scissorsButton = findViewById(R.id.scissorsButton);
        rockButton = findViewById(R.id.rockButton);
        spockButton = findViewById(R.id.spockButton);
        lizardButton = findViewById(R.id.lizardButton);
        enemyChoice = findViewById(R.id.enemy_choice);

        // REMOVING DIFFICULTY FROM PREGAMEACTIVITY
        int difficulty = getIntent().getIntExtra("difficulty", 3);

        String difficultyLevel;

        // SETTING GAME PROBAS FOR SELECTED VALUES
        switch (difficulty) {
            case 1:
                difficultyLevel = "EASY";
                probaWin = 0.7;
                probaDraw = 0.8;
                pointsGained = 1;
                break;
            case 2:
                difficultyLevel = "MEDIUM";
                probaWin = 0.45;
                probaDraw = 0.55;
                pointsGained = 4;
                break;
            case 3:
            default:
                difficultyLevel = "EXTREME";
                probaWin = 0.2;
                probaDraw = 0.3;
                pointsGained = 12;
                break;
        }

        // USER INFO
        gameDifficultyTextView.setText("DIFFICULTY : " + difficultyLevel);

        SetOnClickListeners();
    }

    /**
     * ADDING SIGNS ON CLICK LISTENERS
     */
    private void SetOnClickListeners() {

        // CREATE ONCLICKLISTENERS FOR EACH BUTTON
        AddValuedClickListener(spockButton,SignEnum.SPOCK);
        AddValuedClickListener(lizardButton,SignEnum.LIZARD);
        AddValuedClickListener(paperButton,SignEnum.PAPER);
        AddValuedClickListener(rockButton,SignEnum.ROCK);
        AddValuedClickListener(scissorsButton,SignEnum.SCISSORS);
    }

    /**
     * ASSOCIATE EACH BUTTON TO ITS SIGN VALUE
     * @param imageButton
     * @param signEnum
     */
    private void AddValuedClickListener(ImageButton imageButton, final SignEnum signEnum)
    {
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                double probaResult = Math.random();

                playerSign = signEnum;

                // WIN
                if (probaResult <= probaWin){
                    nbpoints++;
                    DisplayEnemyChoice(RoundResultEnum.WIN);
                    turnNumber++;
                    Toast.makeText(getApplicationContext(),"YOU WON THIS ROUND",Toast.LENGTH_SHORT).show();
                }
                // DRAW
                else if(probaResult <= probaDraw){
                    DisplayEnemyChoice(RoundResultEnum.DRAW);
                    Toast.makeText(getApplicationContext(),"DRAW",Toast.LENGTH_SHORT).show();
                }
                // DEFEAT
                else{
                    turnNumber++;
                    DisplayEnemyChoice(RoundResultEnum.DEFEAT);
                    Toast.makeText(getApplicationContext(),"YOU LOST THIS ROUND",Toast.LENGTH_SHORT).show();
                }

                UpdateUI();

                // VERIFYING IF MATCH IS OVER
                if (turnNumber == 4 && (nbpoints == 0 || nbpoints == 3))
                        GetMatchResult();
                else if(turnNumber > 4 && (nbpoints == 3 || ((turnNumber - 1) - nbpoints) == 3))
                        GetMatchResult();
            }
        });
    }

    /**
     * GET WINNING PLAYER AND ADD POINTS IF NECESSARY
     */
    private void GetMatchResult(){
        if(nbpoints == 3) {
            Toast.makeText(getApplicationContext(), "YOU HAVE WON " + pointsGained + " POINTS !!", Toast.LENGTH_SHORT).show();

            userDatabaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    // AVOID POINTS BEING MULTIPLE TIMES
                    if(!hasBeenAdded) {
                        try {
                            int newScore = Integer.parseInt(dataSnapshot.child("score").getValue().toString());
                            newScore -= pointsGained;
                            userDatabaseReference.child("score").setValue(newScore);
                            hasBeenAdded = true;
                        } catch (Exception ex) {
                            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        finish();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        else {
            Toast.makeText(getApplicationContext(), "YOU HAVE LOST !!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    /**
     * DISPLAY GAME INFOS
     */
    private void UpdateUI(){
        pointsTextViex.setText(nbpoints + " Points");
        roundTextView.setText("ROUND : " + turnNumber + "/5");
    }

    /**
     * DISPLAY CORRESPONDING SIGN PLAYED BY THE COMPUTER
     * @param roundResultEnum
     */
    private void DisplayEnemyChoice(RoundResultEnum roundResultEnum){

        boolean firstSign = randomSign.nextBoolean();

        switch (playerSign){
            case ROCK:
                switch (roundResultEnum){
                    case WIN:
                        if(firstSign)
                            enemyChoice.setImageResource(R.drawable.and_lizard);
                        else
                            enemyChoice.setImageResource(R.drawable.and_scissors);
                        break;
                    case DRAW:
                        enemyChoice.setImageResource(R.drawable.and_rock);
                        break;
                    case DEFEAT:
                        if(firstSign)
                            enemyChoice.setImageResource(R.drawable.and_spock);
                        else
                            enemyChoice.setImageResource(R.drawable.and_paper);
                        break;
                }
                break;
            case PAPER:
                switch (roundResultEnum){
                    case WIN:
                        if(firstSign)
                            enemyChoice.setImageResource(R.drawable.and_rock);
                        else
                            enemyChoice.setImageResource(R.drawable.and_spock);
                        break;
                    case DRAW:
                        enemyChoice.setImageResource(R.drawable.and_paper);
                        break;
                    case DEFEAT:
                        if(firstSign)
                            enemyChoice.setImageResource(R.drawable.and_scissors);
                        else
                            enemyChoice.setImageResource(R.drawable.and_lizard);
                        break;
                }
                break;
            case SCISSORS:
                switch (roundResultEnum){
                    case WIN:
                        if(firstSign)
                            enemyChoice.setImageResource(R.drawable.and_lizard);
                        else
                            enemyChoice.setImageResource(R.drawable.and_paper);
                        break;
                    case DRAW:
                        enemyChoice.setImageResource(R.drawable.and_scissors);
                        break;
                    case DEFEAT:
                        if(firstSign)
                            enemyChoice.setImageResource(R.drawable.and_spock);
                        else
                            enemyChoice.setImageResource(R.drawable.and_rock);
                        break;
                }
                break;
            case LIZARD:
                switch (roundResultEnum){
                    case WIN:
                        if(firstSign)
                            enemyChoice.setImageResource(R.drawable.and_spock);
                        else
                            enemyChoice.setImageResource(R.drawable.and_paper);
                        break;
                    case DRAW:
                        enemyChoice.setImageResource(R.drawable.and_lizard);
                        break;
                    case DEFEAT:
                        if(firstSign)
                            enemyChoice.setImageResource(R.drawable.and_rock);
                        else
                            enemyChoice.setImageResource(R.drawable.and_scissors);
                        break;
                }
                break;
            case SPOCK:
                switch (roundResultEnum){
                    case WIN:
                        if(firstSign)
                            enemyChoice.setImageResource(R.drawable.and_rock);
                        else
                            enemyChoice.setImageResource(R.drawable.and_scissors);
                        break;
                    case DRAW:
                        enemyChoice.setImageResource(R.drawable.and_scissors);
                        break;
                    case DEFEAT:
                        if(firstSign)
                            enemyChoice.setImageResource(R.drawable.and_paper);
                        else
                            enemyChoice.setImageResource(R.drawable.and_lizard);
                        break;
                }
                break;
        }
    }
}
