package com.example.battleapp;

public enum SignEnum {
    SCISSORS,
    PAPER,
    ROCK,
    SPOCK,
    LIZARD
}
