package com.example.battleapp;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class PreGameActivity extends UserActivity {

    private TextView userHello;
    private DatabaseReference userDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(getUserId());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_game);

        // INITIALIZING WIDGETS REFS
        Button rulesButton = findViewById(R.id.button_rules);
        Button deconnectionButton = findViewById(R.id.button_disconnect);
        Button playButton = findViewById(R.id.button_play);
        Button ranksButton = findViewById(R.id.button_ranks);
        userHello = findViewById(R.id.textbonjour_pre_game);

        DisplayUserName();

        rulesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Redirecting to rules page",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://pausegeek.fr/dossiers/4-apprendre-a-jouer-a-pierre-feuille-ciseaux-lezard-spock.geek")));
            }
        });

        deconnectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                System.exit(0);
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PreGameActivity.this, GameSettingActivity.class));
            }
        });

        ranksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), RanksActivity.class));
            }
        });
    }

    /**
     * DISPLAY ACTUAL USER NAME
     */
    private void DisplayUserName(){
        DatabaseReference userData = userDatabaseReference;

        userData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    final String userName = dataSnapshot.child("name").getValue().toString();
                    userHello.setText("Hello " + userName);
                }catch (NullPointerException ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
