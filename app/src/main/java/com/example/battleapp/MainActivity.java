package com.example.battleapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
// import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    //private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // INITIALIZING WIDGETS REFS
        Button connexionButton = findViewById(R.id.connexion_button);
        Button inscriptionButton = findViewById(R.id.inscription_button);

        //mediaPlayer = MediaPlayer.create(getApplicationContext(),R.raw.menusong);

        // INITIALIZING BUTTONS CLICK EVENT
        connexionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent connexionIntent = new Intent(getApplicationContext(), FormConnectionActivity.class);
                startActivity(connexionIntent);
            }
        });

        inscriptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),FormInscriptionActivity.class));
            }
        });

        /*
        if(!mediaPlayer.isPlaying())
            mediaPlayer.start();
         */
    }
}
