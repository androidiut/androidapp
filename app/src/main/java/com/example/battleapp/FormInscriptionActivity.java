package com.example.battleapp;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class FormInscriptionActivity extends UserActivity {

    private EditText formInscriptionName,formInscriptionFirstName,formInscriptionEmail,formInscriptionPassword;
    private Spinner genderSpinner,ageSpinner;
    private DatabaseReference userDatabase;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_inscription);

        // INITIALIZING WIDGETS REFS
        Button formInscriptionButton = findViewById(R.id.form_inscription_button);
        ImageButton formInscriptionBackButton = findViewById(R.id.form_inscription_back_button);
        formInscriptionName =       findViewById(R.id.form_inscription_nom);
        formInscriptionFirstName =  findViewById(R.id.form_inscription_prenom);
        formInscriptionEmail =      findViewById(R.id.form_inscription_email);
        formInscriptionPassword =   findViewById(R.id.form_inscription_passwd_tv);
        genderSpinner =             findViewById(R.id.gender_spinner);
        ageSpinner =                findViewById(R.id.age_spinner);

        // INITIALIZING DATABASE CONNECTION
        mAuth =                     FirebaseAuth.getInstance();
        userDatabase =              FirebaseDatabase.getInstance().getReference().child("users");


        ArrayList<Integer> ageList = new ArrayList<>();

        // FILLING SPINNER FOR AGE VALUES
        for (int ageValue = 13; ageValue <= 100; ageValue++)
            ageList.add(ageValue);

        ArrayAdapter<Integer> ageSpinnerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, ageList);
        ageSpinner.setAdapter(ageSpinnerAdapter);


        // ADDING CLICKS EVENTS ON WIDGETS
        formInscriptionBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FormInscriptionActivity.this,MainActivity.class));
            }
        });

        formInscriptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String logName = formInscriptionEmail.getText().toString().trim();
                String password = formInscriptionPassword.getText().toString().trim();
                String name = formInscriptionName.getText().toString().trim();
                String firstName = formInscriptionFirstName.getText().toString().trim();

                // VERIFYING EMAIL PATTERN
                if(!Patterns.EMAIL_ADDRESS.matcher(logName).matches()){
                    Toast.makeText(FormInscriptionActivity.this, "This email address can't exist...",Toast.LENGTH_SHORT).show();
                    formInscriptionEmail.setError("Enter valid login");
                    return;
                }

                // VERIFYING EMAIL ADDRESS
                if(TextUtils.isEmpty(logName)){
                    Toast.makeText(FormInscriptionActivity.this, "Login cannot be empty",Toast.LENGTH_SHORT).show();
                    formInscriptionEmail.setError("Enter valid login");
                    return;
                }

                // VERIFYING IF PASSWORD EMPTY
                if(TextUtils.isEmpty(password)){
                    Toast.makeText(FormInscriptionActivity.this, "Enter your password", Toast.LENGTH_SHORT).show();
                    formInscriptionPassword.setError("Enter a valid password");
                    return;
                }

                // VERIFYING IF USERNAME EMPTY
                if(TextUtils.isEmpty(name)){
                    Toast.makeText(FormInscriptionActivity.this, "Name cannot be empty", Toast.LENGTH_SHORT).show();
                    formInscriptionName.setError("Enter a valid name");
                    return;
                }

                // VERIFYING IF USERNAME EMPTY
                if(TextUtils.isEmpty(firstName)){
                    Toast.makeText(FormInscriptionActivity.this, "Firstname cannot be empty", Toast.LENGTH_LONG).show();
                    formInscriptionFirstName.setError("Enter a valid Firstname");
                    return;
                }

                Toast.makeText(FormInscriptionActivity.this, "Trying to create user, please wait...",Toast.LENGTH_SHORT).show();

                // CREATING USER IN FIREBASE AUTHENTICATION
                mAuth.createUserWithEmailAndPassword(logName,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // IF USER ADDED
                        if(task.isSuccessful()){

                            // ADDING USER TO FIREBASE REALTIME DATABASE
                            try {
                                AppUser appUser = new AppUser(mAuth.getCurrentUser().getUid(), formInscriptionFirstName.getText().toString().trim(), formInscriptionName.getText().toString().trim(), Integer.parseInt(ageSpinner.getSelectedItem().toString()), genderSpinner.getSelectedItem().toString(), logName);
                                userDatabase.child(appUser.getId()).setValue(appUser);

                                setUserId(mAuth.getUid());

                                // UI INFO
                                Toast.makeText(FormInscriptionActivity.this, "Creation succesful", Toast.LENGTH_SHORT).show();

                                // STARTING GAME MENU ACTIVITY
                                startActivity(new Intent(FormInscriptionActivity.this, PreGameActivity.class));
                            }
                            catch (NullPointerException ex)
                            {
                                ex.printStackTrace();
                            }
                        }
                        // IF USER COULDN'T ADD
                        else{
                            Toast.makeText(FormInscriptionActivity.this, "Creation failed, your account may already exists...",Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }
}
