package com.example.battleapp;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class FormConnectionActivity extends UserActivity {

    private TextView loginTv, passwordTv;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_connection);

        // INITIALIZING WIDGETS REFS
        Button signInButton = findViewById(R.id.form_connection_button);
        ImageButton backButton = findViewById(R.id.form_back_button);
        passwordTv   = findViewById(R.id.form_passwd_tv);
        loginTv      = findViewById(R.id.form_login_tv);

        // INITIALIZING BUTTONS CLICK EVENT
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String logName = loginTv.getText().toString().trim();
                String password = passwordTv.getText().toString().trim();

                // VERIFYING USER ENTRIES
                if(TextUtils.isEmpty(logName)){
                    Toast.makeText(getApplicationContext(), "Login cannot be empty",Toast.LENGTH_SHORT).show();
                    loginTv.setError("Enter valid login");
                    return;
                }

                if(TextUtils.isEmpty(password)){
                    Toast.makeText(getApplicationContext(), "Enter your password", Toast.LENGTH_SHORT).show();
                    passwordTv.setError("Enter a valid password");
                    return;
                }

                if(!Patterns.EMAIL_ADDRESS.matcher(logName).matches()){
                    Toast.makeText(getApplicationContext(), "This email address can't exist...",Toast.LENGTH_SHORT).show();
                    loginTv.setError("Enter valid login");
                    return;
                }

                // USER FEEDBACK
                Toast.makeText(FormConnectionActivity.this, "Trying to connect, please wait...",Toast.LENGTH_SHORT).show();

                // TRYING TO CONNECT TO EXISTING ACCOUNT
                mAuth.signInWithEmailAndPassword(logName, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // CONNECTION SUCCESSFUL
                        if(task.isSuccessful()){
                            setUserId(mAuth.getUid()); // STOCKING USER ID IN APP
                            Toast.makeText(getApplicationContext(), "Connected successfully", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), PreGameActivity.class)); // LAUNCHING NEW ACTIVITY
                        }
                        // CONNECTION ERROR
                        else{
                            try {
                                Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            }
                            catch (NullPointerException ex)
                            {
                                ex.printStackTrace();
                            }
                        }
                    }
                });
            }
        });
    }
}
