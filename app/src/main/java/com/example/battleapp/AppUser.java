package com.example.battleapp;

// CLASS MADE FOR SENDING USERS DATA TO REALTIME DATABASE
public class AppUser {

    // ATTRIBUTES
    private String id;
    private String name;
    private String lastName;
    private int age;
    private String gender;
    private String mailAddress;
    private int score;

    // CONSTRUCTORS
    AppUser() {}

    AppUser(String id, String name, String lastName, int age, String gender, String mailAddress) {

        this.id = id;

        this.name = name;

        this.lastName = lastName;

        if (age > 0 && age < 120)
            this.age = age;
        else
            throw new IllegalArgumentException();

        if (gender.equals("M") || gender.equals("F"))
            this.gender = gender;
        else
            throw new IllegalArgumentException();

        this.mailAddress = mailAddress;

        score = 0;
    }

    // GETTERS
    public String getId() { return  id; }

    public String getName() { return name; }

    public String getLastName() { return lastName; }

    public int getAge() { return age; }

    public String getGender() { return gender; }

    public String getMailAddress() { return mailAddress; }

    public int getScore() { return score; }
}
