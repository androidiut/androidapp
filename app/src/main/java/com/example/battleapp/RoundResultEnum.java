package com.example.battleapp;

public enum RoundResultEnum {
    WIN,
    DEFEAT,
    DRAW
}
